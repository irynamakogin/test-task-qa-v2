import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Point from './Point';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    minWidth: 400,
  },
}));

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

const Compare = ({products}) => {
  const classes = useStyles();
  let checkCompare = products.length === 2 && products.some(item => parseInt(item.id) === 2) && products.some(item => parseInt(item.id) === 3);
  const getWaterproof = (product, check) => {
    if(check && parseInt(product.id) === 2) {
      return NaN;
    } 
    return product.waterproof;
  }
  const isFox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
  const getDisplay = (product, check) => {
    if(check && parseInt(product.id) === 2) {
      return '';
    } 
    return product.display;
  }
  return (
    <Container maxWidth="md">
      <Grid container spacing={3}>
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <StyledTableCell/>
                {products.map(product =>
                  <StyledTableCell key={product.id} align="center">
                    <Typography>
                      {product.name}
                    </Typography>
                  </StyledTableCell>
                )}
              </TableRow>
            </TableHead>
            <TableBody>
                <StyledTableRow>
                  <StyledTableCell component="th" scope="row">
                    Price
                  </StyledTableCell>
                  {products.map(product =>
                    <StyledTableCell key={product.id} align="center">
                      <Typography>
                        {product.priceMin} - {parseInt(product.id) === 4 ? 39000: product.priceMax} UAH
                      </Typography>
                    </StyledTableCell>
                  )}
                </StyledTableRow>
                <StyledTableRow>
                  <StyledTableCell component="th" scope="row">
                    Colors
                  </StyledTableCell>
                  {products.map(product =>
                    <StyledTableCell key={product.id} align="center">
                      {product.colors.map((color) => 
                        <Point key={color} color={color} />
                      )}
                    </StyledTableCell>
                  )}
                </StyledTableRow>
                <StyledTableRow>
                  <StyledTableCell component="th" scope="row">
                    Communication standards
                  </StyledTableCell>
                  {products.map(product =>
                    <StyledTableCell key={product.id} align="center">
                      {product.communicationStandards.map((standart) => 
                        <Typography key={standart}>
                          {standart}
                        </Typography>
                      )}
                    </StyledTableCell>
                  )}
                </StyledTableRow>
                <StyledTableRow>
                  <StyledTableCell component="th" scope="row">
                    SIM
                  </StyledTableCell>
                  {products.map(product =>
                    <StyledTableCell key={product.id} align="center">
                      <Typography>
                        {product.SIMCards} SIM
                      </Typography>
                    </StyledTableCell>
                  )}
                </StyledTableRow>
                <StyledTableRow>
                  <StyledTableCell component="th" scope="row">
                    Display
                  </StyledTableCell>
                  {products.map(product =>
                    <StyledTableCell key={product.id} align="center">
                      <Typography>
                        {getDisplay(product, isFox)}''
                      </Typography>
                    </StyledTableCell>
                  )}
                </StyledTableRow>
                <StyledTableRow>
                  <StyledTableCell component="th" scope="row">
                    Memory
                  </StyledTableCell>
                  {products.map(product =>
                    <StyledTableCell key={product.id} align="center">
                      <Typography>
                        {product.memory} Gb
                      </Typography>
                    </StyledTableCell>
                  )}
                </StyledTableRow>
                <StyledTableRow>
                  <StyledTableCell component="th" scope="row">
                    Waterproof
                  </StyledTableCell>
                  {products.map(product =>
                    <StyledTableCell key={product.id} align="center">
                      <Typography>
                        IP{getWaterproof(product, checkCompare)}
                      </Typography>
                    </StyledTableCell>
                  )}
                </StyledTableRow>
            </TableBody>
          </Table>
        </Paper>
      </Grid>
    </Container>
  );
}

export default Compare;
