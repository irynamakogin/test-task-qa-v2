import * as types from '../constants/types';
import axios from 'axios';

export const getProducts = () =>
  dispatch =>
    axios.get('api/v1/products')
      .then(response => {
        dispatch({
          type: types.FETCH_PRODUCTS,
          payload: response.data
        })
      })

export const compare = product => ({
    type: types.COMPARE_PRODUCT,
    product
});