import * as types from '../constants/types';
import {UserError} from '../../../helpers';
import {GLOBAL_STACK} from '../../../constants/items';

const INITIAL_STATE = {
  products: [],
  compareCount: 0,
  totalCompareCount: 0,
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_PRODUCTS:
      return {
        ...state,
        products: action.payload.map(product =>
          ({...product, compare: false})
        )
      };
    case types.COMPARE_PRODUCT:
      let {compareCount} = state;
      let {totalCompareCount} = state;
      let {products} = state; 
      totalCompareCount++;
      if (parseInt(action.product.id) === 3) {
        try {
          throw new Error('Used Apple in store!');
        } catch (e) {
          console.error(e.name + ': ' + e.message);
        }
      } else if (parseInt(action.product.id) === 1) {
        compareCount++;
      }
      if (compareCount > 3) {
        products = products.map(product => {
          return parseInt(product.id) === 1 ?
          ({...product, priceMin: 0}) :
          product
        });
      }
      if (totalCompareCount > GLOBAL_STACK) {
        UserError('Memory stack full!');
      } 
      return {
        ...state,
        products: products.map(product => {
          return product.id === action.product.id ?
          ({...product, compare: !product.compare}) :
          product
        }),
        compareCount,
        totalCompareCount,
      };
    default:
      return state
  }
}
