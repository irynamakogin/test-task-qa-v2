import React from 'react';
import AuthUserContext from './context';
import { withFirebase } from '../Firebase';
import LinearProgress from '@material-ui/core/LinearProgress';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
     },
  }));

const Loader = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <LinearProgress />
        </div> 
    );
}

const withAuthentication = Component => {
  class WithAuthentication extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            authUser: null,
            isLoad: false
        };
    }
  
    componentDidMount() {
        this.listener = this.props.firebase.auth.onAuthStateChanged(
            authUser => {
            authUser
                ? this.setState({ 
                    authUser,
                    isLoad: true
                 })
                : this.setState({ 
                    authUser: null,
                    isLoad: true
                 });
            },
        );
    }
  
    componentWillUnmount() {
      this.listener();
    }
  
    render() {
        if (!this.state.isLoad) {
            return (
                <Loader/>
            );
        }
        return (
            <AuthUserContext.Provider value={this.state.authUser}>
                <Component {...this.props} auth={this.state.authUser}/>
            </AuthUserContext.Provider>
        );
    }
  }

  return withFirebase(WithAuthentication);
};

export default withAuthentication;